'use strict';

var angular = require('angular');

angular.module("dashboard-module")
    .controller('dashboardController', require('./dashboard.controller'));