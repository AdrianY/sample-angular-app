'use strict'
var angular = require('angular');

angular.module("dashboard-module", ["ngRoute"]);

require('./config');
require('./controllers');