function DashBoardModuleRoutes($routeProvider) {
    $routeProvider
        .when("/profile-dashboard", {
            templateUrl : "/app/dash-board-profile/views/profile-dashboard.html"
        })
        .when("/", {
            templateUrl : "/app/dash-board-profile/views/profile-dashboard.html"
        });
}

module.exports = DashBoardModuleRoutes;