var webpack = require('webpack');

module.exports = {
    context: __dirname + '/app',
    entry: {
        app: './app.js',
        vendor: ['angular','angular-route']  
    },
    output: {
        path: __dirname + '/build',
        filename: '[name].bundle.js'
    },
    optimization: {
		splitChunks: {
			cacheGroups: {
				app: {
					name: "app",
					enforce: true
				}
			}
		}
	}
};